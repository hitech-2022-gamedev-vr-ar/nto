using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float rotateSpeed = 10f;

    private Vector3 direction;
    private Vector3 stepRotate;

    // Update is called once per frame
    private void Update()
    {
        direction = (target.position - transform.position);
        stepRotate = Vector3.RotateTowards(transform.forward, direction, rotateSpeed * Time.deltaTime, 0f);
        transform.rotation = Quaternion.LookRotation(stepRotate);
    }
}
